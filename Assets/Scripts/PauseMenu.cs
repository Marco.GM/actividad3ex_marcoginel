using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    // Start is called before the first frame update
    public Player pl;
    public GameObject pauseScreen;
    public bool isPaused;

    public AudioClip hoverSound;
    public AudioClip clickSound;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !pl.isInWinScr)
        {
            Debug.Log("works");
            if(!isPaused)
            {
                Pause();
            } else
            {
                Resume();
            }
        }
    }

    public void Pause()
    {
        isPaused = true;
        pauseScreen.SetActive(true);
        Time.timeScale = 0f;
    }

    public void Resume()
    {
        isPaused = false;
        pauseScreen.SetActive(false);
        Time.timeScale = 1f;
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void playHoverSound()
    {
        SoundFXManager.instance.PlaySoundFXClip(hoverSound, transform, 0.4f);
    }

    public void playClickSound()
    {
        SoundFXManager.instance.PlaySoundFXClip(clickSound, transform, 0.4f);
    }
}
