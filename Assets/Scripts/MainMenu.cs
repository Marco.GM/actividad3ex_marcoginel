using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    //Sorry por la variable est�tica, solo sirve para detectar si es la primera vez en el men� principal :)
    public static bool firstLaunch = true;
    public GameObject[] buttons;
    public GameObject startText;

    public AudioClip hoverSound;
    public AudioClip clickSound;

    private void Start()
    {
        Time.timeScale = 1.0f;
        if (firstLaunch) 
        { 
            disableMenu();
            startText.SetActive(true);
            
        } else
        {
            enableMenu();
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.X) && (firstLaunch))
        {
            enableMenu();
            firstLaunch = false;
        }
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void disableMenu()
    {
        for (int i = 0; i < buttons.Length; i++ ) 
        {
            buttons[i].SetActive(false);
        }
    }

    public void enableMenu()
    {
        if (startText != null)
        {
            startText.SetActive(false);
        }
        
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].SetActive(true);
        }
    }

    public void playHoverSound()
    {
        SoundFXManager.instance.PlaySoundFXClip(hoverSound, transform, 0.4f);
    }

    public void playClickSound()
    {
        SoundFXManager.instance.PlaySoundFXClip(clickSound, transform, 0.4f);
    }

    public void SetFullScreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }
}
