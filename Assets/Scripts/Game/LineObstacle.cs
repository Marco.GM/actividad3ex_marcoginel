using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineObstacle : MonoBehaviour
{
    // Start is called before the first frame update
    

    // Update is called once per frame
    void Update()
    {
        
    }

    private void changeTagCyan()
    {
        transform.gameObject.tag = "Cyan";
    }

    private void changeTagYellow()
    {
        transform.gameObject.tag = "Yellow";
    }

    private void changeTagPink()
    {
        transform.gameObject.tag = "Pink";
    }

    private void changeTagPurple()
    {
        transform.gameObject.tag = "Purple";
    }
}
