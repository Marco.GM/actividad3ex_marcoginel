using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    
    public float jumpForce = 8f;
    Rigidbody2D rb;
    SpriteRenderer sr;
    public GameObject controlsText;
    public PauseMenu pauseMenu;
    public string currentColor;
    public LevelLoader lvlLoad;
    public GameObject winScr;
    public AudioClip jump;
    public AudioClip changeColor;
    public AudioClip win;
    public bool isInWinScr;
    bool firstInput;

    public Color colorCyan;
    public Color colorYellow;
    public Color colorPink;
    public Color colorPurple;

    private void Start()
    {
        isInWinScr = false;
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        firstInput = false;
        rb.gravityScale = 0;
        SetRandomColor();
    }

    private void Awake()
    {
        Time.timeScale = 1f;
    }

    void Update()
    {
        if ((Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0)) && !pauseMenu.isPaused && !isInWinScr)
        {
            rb.velocity = Vector3.up * jumpForce;
            SoundFXManager.instance.PlaySoundFXClip(jump, transform, 0.4f);
            if (!firstInput)
            {
                rb.gravityScale = 3f;
                if (controlsText != null)
                {
                    controlsText.SetActive(false);
                }
                firstInput = true;
            }   
        }

        

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Restart")
        {
            lvlLoad.LoadLevel(SceneManager.GetActiveScene().buildIndex);
        }

        if (other.tag == "ColorChanger")
        {
            SetRandomColor();
            //other.gameObject.SetActive(false);
            Destroy(other.gameObject);  
            SoundFXManager.instance.PlaySoundFXClip(changeColor, transform, 0.15f);    
            return;
        }

        if (other.tag == "Win")
        {
            SoundFXManager.instance.PlaySoundFXClip(win, transform, 0.25f);
            winScr.SetActive(true);
            isInWinScr = true;
            Time.timeScale = 0f;
            return;
        }

        if (other.tag == "FinishLine")
        {
            lvlLoad.LoadLevel(SceneManager.GetActiveScene().buildIndex+1);
            return;
        }

        if (other.tag != currentColor)
        {
            Debug.Log("GO");
            lvlLoad.LoadLevel(SceneManager.GetActiveScene().buildIndex);
        }



       

    }

    void SetRandomColor()
    {
        int index = Random.Range(0, 4);

        switch (index)
        {
            case 0:
                currentColor = "Cyan";
                sr.color = colorCyan;
                break;
            case 1:
                currentColor = "Yellow";
                sr.color = colorYellow;
                break;
            case 2:
                currentColor = "Pink";
                sr.color = colorPink;
                break;
            case 3:
                currentColor = "Purple";
                sr.color = colorPurple;
                break;
            default: break;
        }
    }
}
