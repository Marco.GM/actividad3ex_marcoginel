using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundFXManager : MonoBehaviour
{
    public static SoundFXManager instance;

    [SerializeField] private AudioSource soundFXObject;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public void PlaySoundFXClip(AudioClip audioClip, Transform spawnTransform, float volume)
    {
        //Crear Gameobject
        AudioSource audioSource = Instantiate(soundFXObject, spawnTransform.position, Quaternion.identity);
        //Asignar clip de audio
        audioSource.clip = audioClip;
        //Asignar volumen
        audioSource.volume = volume;
        //Ejecutar sonido
        audioSource.Play();
        //Duracion del sonido
        float clipLenght = audioSource.clip.length;
        //Destruir GameOnject
        Destroy(audioSource.gameObject, clipLenght);
    }

    public void PlayRandomSoundFXClip(AudioClip[] audioClip, Transform spawnTransform, float volume)
    {
        //Sonido Random
        int rand = Random.Range(0, audioClip.Length);
        //Crear Gameobject
        AudioSource audioSource = Instantiate(soundFXObject, spawnTransform.position, Quaternion.identity);
        //Asignar clip de audio
        audioSource.clip = audioClip[rand];
        //Asignar volumen
        audioSource.volume = volume;
        //Ejecutar sonido
        audioSource.Play();
        //Duracion del sonido
        float clipLenght = audioSource.clip.length;
        //Destruir GameOnject
        Destroy(audioSource.gameObject, clipLenght);
    }
}
